const path = require('path')
const webpack = require('webpack')
const HtmlPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const {VueLoaderPlugin} = require('vue-loader')

module.exports = {
	entry: path.join(__dirname, 'src'),
	output: {
		filename: 'bundle.js',
		path: path.join(__dirname, 'dist'),
		publicPath: '/'
	},

	devServer: {
		open: true,
		overlay: true,
		hot: true,
		historyApiFallback: true
	},

	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader',
				options: {
					hotReload: true
				}
			},
			// {
			// 	text: /\.css$/,
			// 	use: [MiniCssExtractPlugin.loader, 'css-loader']
			// },
			{
				test: /\.scss$/,
				use: ExtractTextPlugin.extract({
					use: [
						{loader: 'css-loader', options: {minimize: true}},
						{loader: 'sass-loader'}
					]
				})
			}
		]
	},

	plugins: [
		new VueLoaderPlugin(),
		new ExtractTextPlugin('style.css'),
		new HtmlPlugin({
			title: 'My App',
			template: 'template.html',
			filename: 'index.html'
		}),
		new webpack.HotModuleReplacementPlugin()
	],

	resolve: {
		alias: {
			'~': path.join(__dirname, 'src'),
		},
		extensions: ['.js', '.json', '.vue']
	}
}
