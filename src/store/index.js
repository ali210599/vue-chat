import Vue from 'vue'
import Vuex from 'vuex'



import router from '~/routes/'
// import JoinStore from '~/store/JoinStore'
// import ChatStore from '~/store/ChatStore'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    socket: null,
    socketUrl: 'ws://localhost:7777',
    user: null
  },
  
  getters: {
    user(state) {
    
      return state.user
    
    },
    socket(state) {
      
      return state.socket

    }
  },

  mutations: {
    setSocket(state, payload) {
      
      state.socket = payload
    
    },

    setUser(state, payload) {
      state.user = payload
    }
  },
  
  actions: {
    join({state, commit}, payload) {
      
      const {username, uniq, component} = payload
      const socket = new WebSocket(state.socketUrl)
      
      socket.onopen = () => {
        
        commit('setSocket', socket)
        
        socket.send(JSON.stringify({
          type: 'JOIN',
          payload: {username, uniq}
        }))
      
      }

      socket.onmessage = msg => {
        
        const {type, payload} = JSON.parse(msg.data)
        
        if (type === 'JOIN_SUCCESS') {
          
          commit('setUser', payload)
          component.$router.push('/')
        
        }

      }

      // socket.onclose = msg => {
      //   router.push('/join')
      // }

      socket.onerror = err => {

        component.error = 'Server connection error'
      
      }
    }
  }
})

export default store